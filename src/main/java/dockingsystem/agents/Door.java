package dockingsystem.agents;

import dockingsystem.behaviours.DoorBehaviour;
import jade.core.Agent;

public class Door extends Agent {
    private boolean left = true;
    private String lockName = "";


    @Override
    public void setup() {
        String[] splittedName = this.getName().split("-");

        //Get the properties accoring to the name
        if(splittedName.length == 2){
            if("right".equals(splittedName[0])){left=false;}
            lockName = splittedName[1];
        }else{
            takeDown();
        }

        addBehaviour(new DoorBehaviour());
    }

    /**
     * Opens or closes the side according to the correct arguments
     * @param lockName
     * @param side
     */
    public void openSide(String lockName, String side){
        if(this.lockName.equals(lockName)){
            if("left".equals(side)){
                if(left) {
                    lowerDoor();
                }else{
                    elevateDoor();
                }
            }else if ("right".equals(side)){
                if(!left){
                    lowerDoor();
                }else{
                    elevateDoor();
                }
            }
        }
    }

    private void lowerDoor() {
        System.out.println("Lowering door of lock: " + this.getName());
    }

    private void elevateDoor(){
        System.out.println("Elevating door of lock: " + this.getName());
    }
}
