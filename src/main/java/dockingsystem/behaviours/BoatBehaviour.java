package dockingsystem.behaviours;

import dockingsystem.agents.Boat;
import jade.core.AID;
import jade.core.behaviours.Behaviour;
import jade.lang.acl.ACLMessage;

public class BoatBehaviour extends Behaviour {

    private boolean done = false;
    private Boat boat;

    @Override
    public void action() {
        boat = (Boat) myAgent;

        //send request to enter dock (this will place the boat in a queue
        sendEnterRequest();

        //wait for access
        waitForAccess();

        //wait for other side open
        waitForPassThrough();
    }

    /**
     * Request access to a dock
     */
    private void sendEnterRequest(){
        //create a message for the dock
        ACLMessage message = new ACLMessage(ACLMessage.REQUEST);
        message.setContent(String.valueOf(boat.getLength()));
        message.addReceiver(new AID(boat.getDestination(), AID.ISLOCALNAME));

        //send message to the dock
        boat.send(message);
    }

    /**
     * Wait for accces to the lock
     */
    private void waitForAccess(){
        Boolean dockAvailable = false;
        //listening for acknowledgement
        while(!dockAvailable) {
            ACLMessage receivedMessage = myAgent.blockingReceive();

            if(receivedMessage.getPerformative() == ACLMessage.ACCEPT_PROPOSAL){
                System.out.println("Boat " + this.getAgent().getName() + " Is entering the dock");
                dockAvailable = true;
            }

        }
    }

    /**
     * wait for the announcement to pass through the lock
     */
    private void waitForPassThrough(){
        while(true) {
            ACLMessage receivedMessage = myAgent.blockingReceive();
        }
    }

    @Override
    public boolean done() {
        if(done) {
            System.out.println("Boat behaviour done");
        }
        return done;
    }
}
