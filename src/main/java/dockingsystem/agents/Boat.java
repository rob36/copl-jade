package dockingsystem.agents;

import dockingsystem.behaviours.BoatBehaviour;
import jade.core.Agent;

/**
 * Created by rob on 15-1-17.
 */
public class Boat extends Agent {
    private int length;
    private String destination;

    @Override
    public void setup() {
        addBehaviour(new BoatBehaviour());

        length = 5;
        destination = "Enschede";
    }

    @Override
    public void takeDown() {
        System.out.println("This boat has been taken down: " + this.getName());
    }

    public int getLength() {
        return length;
    }

    public String getDestination() {
        return destination;
    }
}
