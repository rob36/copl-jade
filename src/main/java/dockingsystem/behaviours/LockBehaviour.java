package dockingsystem.behaviours;

import dockingsystem.agents.WaterLock;
import jade.core.AID;
import jade.core.behaviours.Behaviour;
import jade.domain.AMSService;
import jade.domain.FIPAAgentManagement.AMSAgentDescription;
import jade.domain.FIPAAgentManagement.SearchConstraints;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LockBehaviour extends Behaviour {
    private boolean done = false;
    private WaterLock lock;
    Map<String, Integer> boatQueue = new HashMap<>();

    @Override
    public void action() {
        if(!(myAgent instanceof WaterLock)){
            System.out.println("Agent should be instance of Lock!");
            done = true;
            return;
        }

        //get the waterlock agent belonging to this behaviour
        lock = (WaterLock) myAgent;

        // open the lock and announce to all agents that the lock is open
        openLeft();

        //Handle incoming messages until lock full
        fillLock();

        //Open the other side of the lock (automatically closes the left door due to this message also being sent to the left door.
        openRight();
    }

    /**
     * Fill up the lock with boats that message the lock
     */
    private void fillLock(){
        int currentLockAmount = 0;
        while(currentLockAmount < lock.getLockSize()) {
            ACLMessage message = myAgent.blockingReceive();
            currentLockAmount  += addBoat(message);
        }
    }

    /**
     * Add a boat to the lock
     * @param message
     * @return
     */
    private int addBoat(ACLMessage message){
        AID boat = message.getSender();
        int size = Integer.valueOf(message.getContent());

        ACLMessage sendMessage = new ACLMessage(ACLMessage.ACCEPT_PROPOSAL);
        sendMessage.addReceiver(boat);

        lock.send(sendMessage);

        return size;
    }

    private void openLeft(){
        System.out.println("Waterlock announce: Opening left door");
        List<AID> otherAgents = getAllAgents();
        ACLMessage message = new ACLMessage(ACLMessage.INFORM);
        message.setContent("open left");

        for(AID aid : otherAgents){
            message.addReceiver(aid);
        }
        lock.send(message);
    }

    private void openRight(){
        List<AID> otherAgents = getAllAgents();
        ACLMessage message = new ACLMessage(ACLMessage.INFORM);
        message.setContent("open right");

        for(AID aid : otherAgents){
            message.addReceiver(aid);
        }
        lock.send(message);
    }

    /**
     * Gets the agent names of all the other agents beside the current agent.
     */
    private List<AID> getAllAgents(){
        List<AID> agents = new ArrayList<>();

        AMSAgentDescription[] agentsDescriptions = null;
        SearchConstraints c = new SearchConstraints();
        c.setMaxResults ((long) -1);
        try {
            agentsDescriptions = AMSService.search( lock, new AMSAgentDescription(), c);

            AID myId = lock.getAID();

            for(int i = 0; i < agentsDescriptions.length; i++){
                AID aid = agentsDescriptions[i].getName();

                if(!myId.equals(aid)){
                    agents.add(aid);
                }
            }
        } catch (FIPAException e) {
            e.printStackTrace();
        }

        return agents;
    }

    @Override
    public boolean done() {
        return done;
    }
}
