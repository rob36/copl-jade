package dockingsystem.agents;

import dockingsystem.behaviours.LockBehaviour;
import jade.core.Agent;

/**
 * Created by rob on 15-1-17.
 */
public class WaterLock extends Agent {
    private int lockSize = 20;

    @Override
    public void setup() {
        addBehaviour(new LockBehaviour());
    }

    @Override
    public void takeDown() {

    }

    public int getLockSize(){
        return lockSize;
    }
}

