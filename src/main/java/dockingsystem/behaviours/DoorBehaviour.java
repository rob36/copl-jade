package dockingsystem.behaviours;

import dockingsystem.agents.Door;
import jade.core.behaviours.Behaviour;
import jade.lang.acl.ACLMessage;

public class DoorBehaviour extends Behaviour {

    @Override
    public void action() {
        Door door = (Door) myAgent;

        //listening for acknowledgement
        ACLMessage receivedMessage = myAgent.blockingReceive();

        String[] arguments = receivedMessage.getContent().split(" ");

        String dockName = receivedMessage.getSender().getName();
        String side = arguments[1];

        door.openSide(dockName, side);
    }

    @Override
    public boolean done() {
        return false;
    }
}
